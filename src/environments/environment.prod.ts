export const environment = {
  production: true,
  backend_url: 'https://gitmate.api.coala.io',
  edition: 'enterprise',
};
